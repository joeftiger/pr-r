use num::Float;
use std::collections::HashMap;
use std::hash::Hash;

/// Counts the most occurrences of labels.
///
/// # Arguments
/// * `array` - The array of (labels, distances)
///
/// # Returns
/// The most occurred label
pub fn most_occurrences<'a, L, F>(array: &[(&'a L, F)]) -> &'a L
where
    L: Hash + Eq + Clone,
    F: Float,
{
    let mut tree = HashMap::new();

    for pair in array.iter() {
        *tree.entry(pair.0).or_insert(0) += 1;
    }

    tree.iter().max_by(|a, b| a.1.cmp(b.1)).unwrap().0
}
