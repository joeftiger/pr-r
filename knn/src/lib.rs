use num::Float;
use pr_core::vecs::{DenseVector, Metric, SparseVector, Vector};

pub mod algorithm;
pub mod choices;
pub mod preprocessing;

pub type FnMetric<T, F> = fn(&T) -> F;

pub type Sparse = SparseVector<u16, f32>;

pub type Dense = DenseVector<f32>;
pub type DenseMetric = FnMetric<Dense, f32>;

pub fn parse_metric<T, F>(arg: Option<String>) -> FnMetric<T, F>
where
    T: Metric<F>,
    F: Float,
{
    arg.map_or_else(
        || Metric::l2_norm as FnMetric<T, F>,
        |c| match c.to_lowercase().as_str() {
            "l1" => Metric::l1_norm as FnMetric<T, F>,
            "l2" => Metric::l2_norm as FnMetric<T, F>,
            _ => panic!("Unknown metric: {}", c),
        },
    )
}
