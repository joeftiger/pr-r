use knn::preprocessing::condense;
use knn::{parse_metric, Dense, Sparse};
use pr_core::csv::CsvReadable;
use pr_core::dataset::Dataset;
use std::fs;

/// # Arguments
/// * [0] - executable
/// * [1] - dataset
/// * [2] - metric (l1, l2) (default = `l2`)
fn main() {
    // skip program name
    let mut args = std::env::args().skip(1);

    let path = args.next().expect("No training set given");
    let metric = parse_metric(args.next());

    let dataset: Dataset<u8, Dense> =
        Dataset::from_csv(fs::read_to_string(path.clone()).unwrap().as_str());
    println!("Dataset size:\t{}", dataset.size());

    println!("Condensing...");
    let condensed = condense(&dataset, metric);
    println!("-- Condensed dataset size:\t{}", condensed.size());

    fs::write(path + "_condensed", condensed.to_csv()).unwrap()
}
