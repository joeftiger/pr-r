use knn::Dense;
use pr_core::csv::CsvReadable;
use pr_core::dataset::{Dataset, Sample};
use std::fs;
use pr_core::vecs::{DenseVector, SparseVector, Vector};

fn main() {
    // skip program name
    let mut args = std::env::args().skip(1);

    let input_path = args.next().expect("No training set given");
    let output_path = args.next().expect("No test set given");

    let train: Dataset<u8, DenseVector<f32>> =
        Dataset::from_csv(fs::read_to_string(input_path).unwrap().as_str());

    let sparse: Vec<Sample<u8, SparseVector<u16, f32>>> = train
        .samples
        .into_iter()
        .map(|sample| Sample::new(sample.label, SparseVector::from(sample.features.into_data().into_vec())))
        .collect();

    fs::write(output_path, Dataset::new(sparse).to_csv()).unwrap()
}
