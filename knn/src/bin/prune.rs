// use std::fs;
// use pr_core::dataset::Dataset;
//
// /// # Arguments
// /// * [0] - executable
// /// * [1] - training set
// /// * [2] - testing set
// /// * [3] - preserve order (default = `true`)
// fn main() {
//     // skip program name
//     let mut args = std::env::args().skip(1);
//
//     let train_path = args.next().expect("No training set given");
//     let test_path = args.next().expect("No test set given");
//     let preserve_order = args.next().map_or_else(|| true, |c| c.parse().unwrap());
//
//     let train: Dataset<u8, f32> = Dataset::from_csv(fs::read_to_string(train_path.clone()).unwrap().as_str()).unwrap();
//     let test: Dataset<u8, f32> = Dataset::from_csv(fs::read_to_string(test_path.clone()).unwrap().as_str()).unwrap();
//     println!("Features size:\t{}", train.samples[0].features.len());
//
//     println!("Pruning...");
//     let (pruned_train, pruned_test) = prune(&train, &test, preserve_order);
//     println!("-- Pruned features size:\t{}", pruned_train.samples[0].features.len());
//
//     fs::write(train_path + "_pruned", pruned_train.to_csv()).unwrap();
//     fs::write(test_path + "_pruned", pruned_test.to_csv()).unwrap();
// }

fn main() {}
