use knn::algorithm::knn;
use knn::{parse_metric, Dense, Sparse};
use pr_core::csv::CsvReadable;
use pr_core::dataset::Dataset;
use std::fs;
use std::time::Instant;

/// # Arguments
/// * [0] - executable
/// * [1] - training set
/// * [2] - testing set
/// * [3] - metric (l1, l2) (default = `l2`)
/// * [4] - k (default = `5`)
fn main() {
    // skip program name
    let mut args = std::env::args().skip(1);

    let train_path = args.next().expect("No training set given");
    let test_path = args.next().expect("No test set given");
    let metric = parse_metric(args.next());
    let k = args.next().map_or_else(|| 5, |c| c.parse().unwrap());

    let train: Dataset<u8, Sparse> =
        Dataset::from_csv(fs::read_to_string(train_path).unwrap().as_str());
    println!("Training set size:\t{}", train.size());
    let test: Dataset<u8, Sparse> =
        Dataset::from_csv(fs::read_to_string(test_path).unwrap().as_str());
    println!("Testing set size:\t{}", test.size());

    println!("Comparing...");
    let mut correct = 0;
    let now = Instant::now();
    for sample in test.samples.iter() {
        if sample.label == knn(&train, &sample.features, metric, k) {
            correct += 1;
        }
    }

    let elapsed = now.elapsed();

    println!(
        "-- Time taken:\t{:#?}\n-- Accuracy:\t{}",
        elapsed,
        correct as f32 / test.samples.len() as f32
    );
}
