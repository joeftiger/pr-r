use crate::choices::most_occurrences;
use crate::FnMetric;
use num::Float;
use partial_sort::PartialSort;
use pr_core::dataset::Dataset;
use pr_core::Label;
use rayon::prelude::*;
use std::ops::Sub;

/// Performs the K-Nearest Neighbours algorithm on the dataset with a given query.
///
/// # Arguments
/// * `dataset` - The dataset for decision making
/// * `query` - The feature vector to query
/// * `metric` - The metric to use for knn distances
/// * `k` - The number of neighbours to compare
///
/// # Returns
/// The classification of `query` (the most occurred label)
pub fn knn<L, T, F>(dataset: &Dataset<L, T>, query: &T, metric: FnMetric<T, F>, k: usize) -> L
where
    L: Label,
    T: Send + Sync + Sub<Output = T> + Clone,
    F: Float + Send + Sync,
{
    assert!(k > 0);

    let mut distances: Vec<(&L, F)> = dataset
        .samples
        .par_iter()
        .map(|sample| {
            (
                &sample.label,
                metric(&sample.features.clone().sub(query.clone())),
            )
        })
        .collect();

    if k == 1 {
        distances
            .par_iter()
            .min_by(|(_, a), (_, b)| a.partial_cmp(b).unwrap())
            .unwrap()
            .0
            .clone()
    } else {
        distances.partial_sort(k, |(_, a), (_, b)| a.partial_cmp(b).unwrap());

        let nearest = &distances[0..k];

        most_occurrences(nearest).clone()
    }
}
