// use pr_core::Label;
// use pr_core::dataset::{Dataset, Sample};
// use rayon::prelude::*;
// use pr_core::linalg::{Convertible, Sizable};
// use std::ops::Index;
//
// /// Prunes the samples of both train and test set by removing features that are equal across the board.
// ///
// /// # Arguments
// /// * `train_set` - The training set to prune
// /// * `test_set` - The testing set to prune
// /// * `preserve_order` - Whether to preserve the order of dimension (leads to more swapping)
// ///
// /// # Returns
// /// The pruned datasets
// pub fn prune<S, L: Label, T: Sizable + Index<usize>>(train_set: &Dataset<L, T>, test_set: &Dataset<L, T>, preserve_order: bool) -> (Dataset<L, T>, Dataset<L, T>) {
//     let mut same_val_indices = Vec::new();
//
//     let feature_size = train_set.samples[0].features.size();
//
//     for i in 0..feature_size {
//         let mut iter = train_set.samples.iter();
//
//         let eq = &iter.next().unwrap().features[i];
//
//         if iter.chain(test_set.samples.iter()).all(|s| &s.features[i] == eq) {
//             same_val_indices.push(i);
//         }
//     }
//
//     let new_train_set = remove_indices(train_set, &same_val_indices, preserve_order);
//     let new_test_set = remove_indices(test_set, &same_val_indices, preserve_order);
//
//     (new_train_set, new_test_set)
// }
//
// /// Prunes a dataset with given indices.
// ///
// /// # Arguments
// /// * `set` - The dataset to prune
// /// * `indices` - The indices of features to remove
// /// * `preserve_order` - Whether to preserve the order of dimension (leads to more swapping)
// ///
// /// # Returns
// /// The pruned dataset
// fn remove_indices<L: Label, T: Clone>(set: &Dataset<L, T>, indices: &[usize], preserve_order: bool) -> Dataset<L, T> {
//     let remove_feature = if preserve_order {
//         |v: &mut Vec<_>, idx: usize| v.remove(idx)
//     } else {
//         |v: &mut Vec<_>, idx: usize| v.swap_remove(idx)
//     };
//
//     let mut new_samples = Vec::with_capacity(set.size());
//
//     for sample in set.samples.iter() {
//         let mut features = sample.features.clone();
//
//         for idx in indices.iter().rev() {
//             remove_feature(&mut features, *idx);
//         }
//
//         let s = Sample::new(
//             sample.label.clone(),
//             features
//         );
//         new_samples.push(s);
//     }
//
//     Dataset::new(new_samples)
// }
