use crate::algorithm::knn;
use crate::FnMetric;
use num::Float;
use pr_core::dataset::Dataset;
use pr_core::Label;
use std::ops::Sub;

/// Condenses a dataset by removing unneeded samples to sample the rest correctly.
///
/// # Arguments
/// * `dataset` - The dataset to condense
/// * `metric` - The metric to use for decisind unneeded samples
///
/// # Returns
/// The condensed dataset
pub fn condense<L, T, F>(dataset: &Dataset<L, T>, metric: FnMetric<T, F>) -> Dataset<L, T>
where
    L: Label,
    T: Clone + Send + Sync + Sub<Output = T>,
    F: Float + Send + Sync,
{
    let mut condensed = Dataset::new(vec![dataset.samples[0].clone()]);

    for sample in dataset.samples.iter().skip(1) {
        if knn(&condensed, &sample.features, metric, 1) != sample.label {
            condensed.samples.push(sample.clone())
        }
    }

    condensed
}
