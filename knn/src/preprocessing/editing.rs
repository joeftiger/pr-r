use crate::algorithm::knn;
use crate::FnMetric;
use num::Float;
use pr_core::dataset::Dataset;
use pr_core::Label;
use std::ops::Sub;

/// Condenses a dataset by removing outliers.
///
/// # Arguments
/// * `dataset` - The dataset to edit
/// * `metric` - The metric to use for deciding outliers
///
/// # Returns
/// The edited dataset
pub fn edit<L, T, F>(dataset: &Dataset<L, T>, metric: FnMetric<T, F>) -> Dataset<L, T>
where
    L: Label,
    T: Clone + Send + Sync + Sub<Output = T>,
    F: Float + Send + Sync,
{
    let mut truncated = dataset.clone();

    let mut outliers = Vec::new();

    let last_index = truncated.size() - 1;
    for i in 0..truncated.size() {
        let potential = truncated.samples.swap_remove(i);

        if knn(&truncated, &potential.features, metric, 3) != potential.label {
            outliers.push(i);
        }

        // push back the `potential` into the same place
        truncated.samples.push(potential);
        truncated.samples.swap(i, last_index);
    }

    for i in outliers.iter().rev() {
        truncated.samples.swap_remove(*i);
    }

    truncated
}
