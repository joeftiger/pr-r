use crate::csv::CsvReadable;
use std::fmt::Debug;
use std::str::FromStr;

/// A simple data sample of
/// * `label` - `L`
/// * `features` - `T`
pub struct Sample<L, T> {
    pub label: L,
    pub features: T,
}

impl<L, T> Sample<L, T> {
    /// Creates a new sample
    ///
    /// # Arguments
    /// * `label` - The label of the sample
    /// * `features` - The feature vector of the sample
    ///
    /// # Returns
    /// New Self
    pub fn new(label: L, features: T) -> Self {
        Self { label, features }
    }
}

impl<L, T> Clone for Sample<L, T>
where
    L: Clone,
    T: Clone,
{
    /// Clones Self.
    ///
    /// # Performance
    /// Depending on the size of `self.features`, the cloning takes "long".
    ///
    /// # Returns
    /// A clone of Self
    fn clone(&self) -> Self {
        Self::new(self.label.clone(), self.features.clone())
    }
}

impl<L, T> CsvReadable for Sample<L, T>
where
    L: FromStr + ToString,
    T: CsvReadable,
    <L as FromStr>::Err: Debug,
{
    fn to_csv(&self) -> String {
        self.label.to_string() + "," + &*self.features.to_csv()
    }

    fn from_csv(csv_content: &str) -> Self
    where
        Self: Sized,
    {
        let mut label_features = csv_content.splitn(2, ',');

        let label = label_features.next().unwrap().parse::<L>().unwrap();
        let features = T::from_csv(label_features.next().unwrap());

        Self::new(label, features)
    }
}

/// A dataset consists of many [Samples](Sample) .
pub struct Dataset<L, T> {
    pub samples: Vec<Sample<L, T>>,
}

impl<L, T> Dataset<L, T> {
    /// Creates a new dataset.
    ///
    /// # Arguments
    /// * `samples` - The samples of the dataset
    ///
    /// # Returns
    /// New Self
    pub fn new(samples: Vec<Sample<L, T>>) -> Self {
        Self { samples }
    }

    pub fn size(&self) -> usize {
        self.samples.len()
    }
}

impl<L: Clone, T: Clone> Clone for Dataset<L, T> {
    /// Clones the whole dataset
    ///
    /// # Performance
    /// Depending on the dataset size, the cloning takes "long".
    ///
    /// # Returns
    /// A clone of Self
    fn clone(&self) -> Self {
        Self::new(self.samples.clone())
    }
}

impl<L, T> CsvReadable for Dataset<L, T>
where
    L: FromStr + ToString,
    T: CsvReadable,
    <L as FromStr>::Err: Debug,
{
    fn to_csv(&self) -> String {
        self.samples
            .iter()
            .map(|s| s.to_csv())
            .collect::<Vec<_>>()
            .join("\n")
    }

    fn from_csv(csv_content: &str) -> Self
    where
        Self: Sized,
    {
        Self::new(
            csv_content
                .lines()
                .map(|line| Sample::from_csv(line))
                .collect(),
        )
    }
}
