use std::hash::Hash;

pub mod csv;
pub mod dataset;
pub mod vecs;

/// A label extends [Hash](Hash), [Eq](Eq), [Clone](Clone) and should be thread-safe for
/// distributing workloads.
pub trait Label: Hash + Eq + Clone + Send + Sync {}
// Implement for all possible candidates
impl<T> Label for T where T: Hash + Eq + Clone + Send + Sync {}
