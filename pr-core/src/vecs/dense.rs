use crate::vecs::{Vector, Metric};
use num::{Num, ToPrimitive, Float};
use std::ops::{Add, AddAssign, Div, DivAssign, Index, IndexMut, Mul, MulAssign, Sub, SubAssign};
use std::slice::SliceIndex;
use crate::csv::CsvReadable;

pub struct DenseVector<T> {
    data: Vec<T>,
}

impl<T> DenseVector<T> {
    pub fn new(data: Vec<T>) -> Self {
        Self { data }
    }
}

impl<T> Vector<T> for DenseVector<T> {
    fn data(&self) -> &[T] {
        &self.data
    }

    fn into_data(self) -> Box<[T]> {
        self.data.into_boxed_slice()
    }
}

impl<T> Metric<T> for DenseVector<T>
where T: Float {
    fn l1_norm(&self) -> T {
        self.data()
            .iter()
            .fold(T::zero(), |acc, next| acc + next.abs())
    }

    fn l2_norm(&self) -> T {
        self.data()
            .iter()
            .fold(T::zero(), |acc, next| next.mul_add(*next, acc))
    }

    fn ln_norm(&self, n: T) -> T {
        self.data()
            .iter()
            .fold(T::zero(), |acc, next| acc + next.abs().powf(n))
            .powf(T::one() / n)
    }

}

impl<I, T> From<Vec<(I, T)>> for DenseVector<T>
where
    I: Num + ToPrimitive + Copy,
    T: Num,
{
    fn from(v: Vec<(I, T)>) -> Self {
        let capacity = v[v.len() - 1].0.to_usize().unwrap();
        let mut data = Vec::with_capacity(capacity);

        let mut i = 0;
        v.into_iter().for_each(|(index, value)| {
            let idx = index.to_usize().unwrap();

            (i..idx).for_each(|_| data.push(T::zero()));

            data.push(value);

            i = idx + 1;
        });

        Self::new(data)
    }
}

impl<T> CsvReadable for DenseVector<T>
where
    T: Num + ToString
{
    fn to_csv(&self) -> String {
        self.data.iter().map(|value| value.to_string()).collect::<Vec<_>>().join(",")
    }

    fn from_csv(csv_content: &str) -> Self where
        Self: Sized {
        Self::new(csv_content.split(',').map(|v| T::from_str_radix(v, 10).unwrap_or_else(|_| panic!())).collect())
    }
}

impl<T, I> Index<I> for DenseVector<T>
where
    I: SliceIndex<[T]>,
{
    type Output = <I as SliceIndex<[T]>>::Output;

    fn index(&self, index: I) -> &Self::Output {
        &self.data[index]
    }
}

impl<T, I> IndexMut<I> for DenseVector<T>
where
    I: SliceIndex<[T]>,
{
    fn index_mut(&mut self, index: I) -> &mut Self::Output {
        &mut self.data[index]
    }
}

// ----------------------------- Addition <Self> ---------------------------------------------------

impl<T> Add for DenseVector<T>
where
    T: Num + Copy,
{
    type Output = Self;

    fn add(mut self, rhs: DenseVector<T>) -> Self::Output {
        self += rhs;

        self
    }
}

impl<T> AddAssign for DenseVector<T>
where
    T: Num + Copy,
{
    fn add_assign(&mut self, rhs: Self) {
        self.data
            .iter_mut()
            .zip(rhs.data)
            .for_each(|(lhs, b)| *lhs = *lhs + b);
    }
}

// ----------------------------- Subtraction <Self> ------------------------------------------------

impl<T> Sub for DenseVector<T>
where
    T: Num + Copy,
{
    type Output = Self;

    fn sub(mut self, rhs: DenseVector<T>) -> Self::Output {
        self -= rhs;

        self
    }
}

impl<T> SubAssign for DenseVector<T>
where
    T: Num + Copy,
{
    fn sub_assign(&mut self, rhs: Self) {
        self.data
            .iter_mut()
            .zip(rhs.data)
            .for_each(|(lhs, b)| *lhs = *lhs - b);
    }
}

// ----------------------------- Multiplication <Self> ---------------------------------------------

impl<T> Mul for DenseVector<T>
where
    T: Num + Copy,
{
    type Output = Self;

    fn mul(mut self, rhs: DenseVector<T>) -> Self::Output {
        self *= rhs;

        self
    }
}

impl<T> MulAssign for DenseVector<T>
where
    T: Num + Copy,
{
    fn mul_assign(&mut self, rhs: Self) {
        self.data
            .iter_mut()
            .zip(rhs.data)
            .for_each(|(lhs, b)| *lhs = *lhs * b);
    }
}

// ----------------------------- Multiplication <T> ------------------------------------------------

impl<T> Mul<T> for DenseVector<T>
where
    T: Num + Copy,
{
    type Output = Self;

    fn mul(mut self, rhs: T) -> Self::Output {
        self *= rhs;

        self
    }
}

impl<T> MulAssign<T> for DenseVector<T>
where
    T: Num + Copy,
{
    fn mul_assign(&mut self, rhs: T) {
        self.data.iter_mut().for_each(|value| *value = *value * rhs);
    }
}

// ----------------------------- Division <Self> ---------------------------------------------------

impl<T> Div for DenseVector<T>
where
    T: Num + Copy,
{
    type Output = Self;

    fn div(mut self, rhs: DenseVector<T>) -> Self::Output {
        self /= rhs;

        self
    }
}

impl<T> DivAssign for DenseVector<T>
where
    T: Num + Copy,
{
    fn div_assign(&mut self, rhs: Self) {
        self.data
            .iter_mut()
            .zip(rhs.data)
            .for_each(|(lhs, b)| *lhs = *lhs / b);
    }
}

// ----------------------------- Multiplication <T> ------------------------------------------------

impl<T> Div<T> for DenseVector<T>
where
    T: Num + Copy,
{
    type Output = Self;

    fn div(mut self, rhs: T) -> Self::Output {
        self /= rhs;

        self
    }
}

impl<T> DivAssign<T> for DenseVector<T>
where
    T: Num + Copy,
{
    fn div_assign(&mut self, rhs: T) {
        self.data.iter_mut().for_each(|value| *value = *value / rhs);
    }
}
