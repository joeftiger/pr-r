use crate::csv::CsvReadable;
use crate::vecs::{Vector, Metric};
use num::{FromPrimitive, Num, Float};
use std::cmp::Ordering;
use std::ops::{Add, AddAssign, Div, DivAssign, Index, Mul, MulAssign, Sub, SubAssign};

pub struct SparseVector<I, T> {
    data: Vec<(I, T)>,
    zero: T,
}

impl<I, T> SparseVector<I, T> {
    pub fn new(data: Vec<(I, T)>, zero: T) -> Self {
        Self { data, zero }
    }
}

impl<I, T> Vector<(I, T)> for SparseVector<I, T> {
    fn data(&self) -> &[(I, T)] {
        &self.data
    }

    fn into_data(self) -> Box<[(I, T)]> {
        self.data.into_boxed_slice()
    }
}

impl<I, T> From<Vec<T>> for SparseVector<I, T>
where
    I: FromPrimitive,
    T: Num,
{
    fn from(v: Vec<T>) -> Self {
        let mut data = Vec::new();

        for (index, value) in v.into_iter().enumerate() {
            if value != T::zero() {
                data.push((I::from_usize(index).unwrap(), value));
            }
        }

        data.shrink_to_fit();

        Self::new(data, T::zero())
    }
}

impl<I, T> Clone for SparseVector<I, T>
where
    I: Clone,
    T: Clone,
{
    fn clone(&self) -> Self {
        Self::new(self.data.clone(), self.zero.clone())
    }
}

impl<I, T> Metric<T> for SparseVector<I, T> where T: Float {
    fn l1_norm(&self) -> T {
        self.data.iter().fold(T::zero(), |acc, (_, next)| acc + next.abs())
    }

    fn l2_norm(&self) -> T {
        self.data.iter().fold(T::zero(), |acc, (_, next)| {
            next.mul_add(*next, acc)
        })
    }

    fn ln_norm(&self, n: T) -> T {
        self.data.iter().fold(T::zero(), |acc, (_, next)| {
            acc + next.abs().powf(n)
        }).powf(T::one() / n)
    }
}

impl<I, T> Index<I> for SparseVector<I, T>
where
    I: Num + Ord + Copy,
{
    type Output = T;

    fn index(&self, index: I) -> &Self::Output {
        match self.data.binary_search_by_key(&index, |(i, _)| *i) {
            Ok(idx) => &self.data[idx].1,
            Err(_) => &self.zero,
        }
    }
}

impl<I, T> CsvReadable for SparseVector<I, T>
where
    I: Num + ToString,
    T: Num + ToString,
{
    fn to_csv(&self) -> String {
        use std::iter::once;

        self.data
            .iter()
            .flat_map(|(index, value)| once(index.to_string()).chain(once(value.to_string())))
            .collect::<Vec<_>>()
            .join(",")
    }

    fn from_csv(csv_content: &str) -> Self
    where
        Self: Sized,
    {
        let mut indices = Vec::new();
        let mut data = Vec::new();

        csv_content
            .split(',')
            .enumerate()
            .for_each(|(index, entry)| match index % 2 {
                0 => indices.push(I::from_str_radix(entry, 10).unwrap_or_else(|_| panic!())),
                _ => data.push(T::from_str_radix(entry, 10).unwrap_or_else(|_| panic!())),
            });

        Self::new(
            indices.into_iter().zip(data.into_iter()).collect(),
            T::zero(),
        )
    }
}

// ----------------------------- Addition <Self> ---------------------------------------------------

impl<I, T> Add for SparseVector<I, T>
where
    I: Num + Ord + Copy,
    T: Num + Copy,
{
    type Output = Self;

    fn add(mut self, rhs: SparseVector<I, T>) -> Self::Output {
        self += rhs;

        self
    }
}

impl<I, T> AddAssign for SparseVector<I, T>
where
    I: Num + Ord + Copy,
    T: Num + Copy,
{
    fn add_assign(&mut self, rhs: Self) {
        let mut i = 0;
        let mut j = 0;

        loop {
            let sparse_lhs = &self.data[i].0;
            let sparse_rhs = &rhs.data[i].0;

            match sparse_lhs.cmp(sparse_rhs) {
                Ordering::Less => {
                    self.data.insert(i, rhs.data[j]);
                    i += 1;
                }
                Ordering::Equal => {
                    self.data[i].1 = self.data[i].1 + rhs.data[j].1;
                    i += 1;
                    j += 1;
                }
                Ordering::Greater => j += 1,
            }

            if i == self.data.len() {
                self.data.extend_from_slice(&rhs.data[j..]);
                break;
            }

            if j == rhs.data.len() {
                break;
            }
        }
    }
}

// ----------------------------- Subtraction <Self> ------------------------------------------------

impl<I, T> Sub for SparseVector<I, T>
where
    I: Num + Ord + Copy,
    T: Num + Copy,
{
    type Output = Self;

    fn sub(mut self, rhs: SparseVector<I, T>) -> Self::Output {
        self -= rhs;

        self
    }
}

impl<I, T> SubAssign for SparseVector<I, T>
where
    I: Num + Ord + Copy,
    T: Num + Copy,
{
    fn sub_assign(&mut self, rhs: Self) {
        let mut i = 0;
        let mut j = 0;

        loop {
            let sparse_lhs = &self.data[i].0;
            let sparse_rhs = &rhs.data[i].0;

            match sparse_lhs.cmp(sparse_rhs) {
                Ordering::Less => {
                    self.data.insert(i, rhs.data[j]);
                    i += 1;
                }
                Ordering::Equal => {
                    self.data[i].1 = self.data[i].1 - rhs.data[j].1;
                    i += 1;
                    j += 1;
                }
                Ordering::Greater => j += 1,
            }

            if i == self.data.len() {
                rhs.data[j..]
                    .iter()
                    .for_each(|(index, value)| self.data.push((*index, T::zero() - *value)));
                break;
            }

            if j == rhs.data.len() {
                break;
            }
        }
    }
}

// ----------------------------- Multiplication <Self> ---------------------------------------------

impl<I, T> Mul for SparseVector<I, T>
where
    I: Num + Ord + Copy,
    T: Num + Copy,
{
    type Output = Self;

    fn mul(mut self, rhs: SparseVector<I, T>) -> Self::Output {
        self *= rhs;

        self
    }
}

impl<I, T> MulAssign for SparseVector<I, T>
where
    I: Num + Ord + Copy,
    T: Num + Copy,
{
    fn mul_assign(&mut self, rhs: Self) {
        let mut i = 0;
        let mut j = 0;

        loop {
            let sparse_lhs = &self.data[i].0;
            let sparse_rhs = &rhs.data[i].0;

            match sparse_lhs.cmp(sparse_rhs) {
                Ordering::Less => {
                    i += 1;
                }
                Ordering::Equal => {
                    self.data[i].1 = self.data[i].1 * rhs.data[j].1;
                    i += 1;
                    j += 1;
                }
                Ordering::Greater => j += 1,
            }

            if i == self.data.len() || j == rhs.data.len() {
                break;
            }
        }
    }
}

// ----------------------------- Multiplication <T> ------------------------------------------------

impl<I, T> Mul<T> for SparseVector<I, T>
where
    I: Num + Ord + Copy,
    T: Num + Copy,
{
    type Output = Self;

    fn mul(mut self, rhs: T) -> Self::Output {
        self *= rhs;

        self
    }
}

impl<I, T> MulAssign<T> for SparseVector<I, T>
where
    I: Num + Ord + Copy,
    T: Num + Copy,
{
    fn mul_assign(&mut self, rhs: T) {
        self.data
            .iter_mut()
            .for_each(|(_, value)| *value = *value * rhs);
    }
}

// ----------------------------- Division <Self> ---------------------------------------------------

impl<I, T> Div for SparseVector<I, T>
where
    I: Num + Ord + Copy,
    T: Num + Copy,
{
    type Output = Self;

    fn div(mut self, rhs: SparseVector<I, T>) -> Self::Output {
        self /= rhs;

        self
    }
}

impl<I, T> DivAssign for SparseVector<I, T>
where
    I: Num + Ord + Copy,
    T: Num + Copy,
{
    fn div_assign(&mut self, rhs: Self) {
        let mut i = 0;
        let mut j = 0;

        loop {
            let sparse_lhs = &self.data[i].0;
            let sparse_rhs = &rhs.data[i].0;

            match sparse_lhs.cmp(sparse_rhs) {
                Ordering::Less => {
                    i += 1;
                }
                Ordering::Equal => {
                    self.data[i].1 = self.data[i].1 / rhs.data[j].1;
                    i += 1;
                    j += 1;
                }
                Ordering::Greater => j += 1,
            }

            if i == self.data.len() || j == rhs.data.len() {
                break;
            }
        }
    }
}

// ----------------------------- Multiplication <T> ------------------------------------------------

impl<I, T> Div<T> for SparseVector<I, T>
where
    I: Num + Ord + Copy,
    T: Num + Copy,
{
    type Output = Self;

    fn div(mut self, rhs: T) -> Self::Output {
        self /= rhs;

        self
    }
}

impl<I, T> DivAssign<T> for SparseVector<I, T>
where
    I: Num + Ord + Copy,
    T: Num + Copy,
{
    fn div_assign(&mut self, rhs: T) {
        self.data
            .iter_mut()
            .for_each(|(_, value)| *value = *value / rhs);
    }
}
