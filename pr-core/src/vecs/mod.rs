use num::Float;

mod dense;
mod sparse;

pub use dense::*;
pub use sparse::*;

pub trait Vector<T> {
    fn data(&self) -> &[T];

    fn into_data(self) -> Box<[T]>;
}

pub trait Metric<T> {
    fn l1_norm(&self) -> T;
    fn l2_norm(&self) -> T;
    fn ln_norm(&self, n: T) -> T;
}