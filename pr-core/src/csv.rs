pub trait CsvReadable {
    fn to_csv(&self) -> String;
    fn from_csv(csv_content: &str) -> Self
    where
        Self: Sized;
}
